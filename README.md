## Features

* [WordPress](https://wordpress.org/) Basic scaffolding
* [Gulp](http://gulpjs.com/) Tasks integration
* [Stylelint](https://stylelint.io/) CSS Linter to have consistency between developers
* [ESlint](https://eslint.org/) JS Linter to have consistency between developers
* [Prettier](https://prettier.io/) Code formatter

## Getting Started
1. Change the **`proxy`** value on `package.json` with the URL that you configured on your local server.
2. Run command `yarn install` on the theme root directory
3. Run command `gulp watch` on the theme root directory for watch
4. Run command `gulp build` on the theme root directory for build
5. Run command `gulp zip` on the theme root directory for zip the theme
6. Start to develop! :D

## Development Guidelines

### Using generated files

When you need add custom scripts, styles or images files just attach in html using `src` folder instead of `assets` folder.

## Production Guidelines

The production deployment process is not very automated but you need to check and follow the next steps for a good deployment process.

1. Check your dependencies! Remove those that you not use and attach the minified files.
2. Check the ESlint to check your code presentation and good practices.
3. Run `gulp build` on your theme root folder. This command minify your scripts and styles and detach the source-maps.
4. Test the theme running with production files to check that everything is alright.

## Improve the process

All help is welcome, if you consider you did something to improve the starter theme or common
reutilizable components, you are free to make a new branch and pull request to give us your ideas.
