<?php
get_header();
?>
    <div class="page-content">
        <div class="background-color__white padding__section">
            <div class="container-fluid wrap">
                <div class="row center-xs">
                    <div class="col-xs-11 col-md-6">
                        <h1  class="margin-bottom__small">Lo sentimos...</h1>
                        <p class="font-size__medium">Lo que estabas buscando no está aquí.</p>

                        <div class="search-wrapper margin-top__mega border-radius__normal">
                            <?php echo get_search_form(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();
