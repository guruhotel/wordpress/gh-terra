<?php
/**
 * Init Wordpress setup
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Get updates for our theme
 */
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/guruhotel/wordpress/gh-terra',
	__FILE__,
	'gh-terra'
);
