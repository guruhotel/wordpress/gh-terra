<?php
    get_header();
?>

    <div class="background-color__white padding__small-section archive-header">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-6">
                    <h2 class="font-size__mega text-color__titles">
                        Buscaste: <em class="font-weight__normal text-color__text"><?php echo get_search_query(); ?></em>
                    </h2>

                    <?php if(!have_posts()) : ?>
                        <p class="font-size__medium">No encontramos lo que estabas buscando.<br> ¡Prueba de nuevo!</p>

                        <div class="search-wrapper margin-top__mega border-radius__normal">
                            <?php echo get_search_form(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php if(have_posts()) : ?>
    <section class="blog-posts">

        <div class="container-fluid wrap">
            <div class="row">
                <?php
                    $post_number = 1;
                    while(have_posts()) : the_post();
                ?>
                    <article title="<?php the_title(); ?>" class="article col-md-4 margin-bottom__big <?php if($post_number == 1 ) echo 'article--big col-md-12'; ?>" data-aos="fade-up" data-aos-delay="<?php echo $post_number*50; ?>">
                        <?php echo get_template_part( 'post-card', null ); ?>
                    </article>
                <?php $post_number++; endwhile; ?>


                <div class="pagination margin-bottom__mega--x">
                    <?php
                        the_posts_pagination(array(
                        'prev_text' => 'Anterior',
                        'next_text' => 'Siguiente',
                    ));
                    ?>
                </div>
            </div>

        </div>

    </section>

    <?php endif; ?>

<?php get_footer();
