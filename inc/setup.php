<?php

if ( ! function_exists( 'wordpress_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 */
	function wordpress_theme_setup() {

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus( array(
            'menu-main' => esc_html__( 'Primary menu', 'gh-apollo' ),
            'menu-footer' => esc_html__( 'Footer menu', 'gh-apollo' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

        if( function_exists('acf_add_options_page') ) {
            acf_add_options_page('Opciones');
        }
	}
endif;
add_action( 'after_setup_theme', 'wordpress_theme_setup' );

/**
 * Register widget area.
 *
 */
function wordpress_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gh-apollo' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gh-apollo' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'wordpress_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wordpress_theme_scripts() {
    wp_enqueue_style( 'gh-apollo-style', get_stylesheet_uri() );
    wp_enqueue_script( 'gh-apollo-scripts', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'wordpress_theme_scripts' );
