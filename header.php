<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&family=Poppins:wght@400;500;700&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/0d7a171506.js" crossorigin="anonymous"></script>
    <meta name="google-site-verification" content="x2KCf9w7xJiH9aBIXGtgQYFp48mOOaLOf-V-MU4_gLw" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136943237-35"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-136943237-35');
    </script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <header id="header" class="headbar padding__big box-shadow__normal">
        <div class="container-fluid wrap">
            <div class="row center-xs middle-xs">
                <div class="brand col-xs-5 col-sm-3 col-md-2">
                    <a href="<?php bloginfo('wpurl'); ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-original.svg">
                    </a>
                </div>
                <div class="brand col-xs-2 col-sm-8 col-md-9 order-xs__3">
                   <nav class="main-menu">
                       <?php wp_nav_menu( array( 'theme_location' => 'menu-main', 'container' => '' ) ); ?>
                   </nav>
                </div>
                <div class="brand col-xs-5 col-sm-1 col-md-1 end-xs">
                    <button type="button" id="toggle-search"><i class="fas fa-search color__main"></i></button>
                </div>
            </div>
        </div>
    </header>

    <div class="search-form-wrapper">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-12 col-sm-10">
                    <?php echo get_search_form(); ?>
                </div>
            </div>
        </div>
    </div>



