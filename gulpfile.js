const { src, dest, watch, series, parallel } = require('gulp');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const del = require('del');
const gulpLoadPlugins = require('gulp-load-plugins');
const server = require('browser-sync');
const pjson = require('./package.json');

const $ = gulpLoadPlugins();
const sourceDir = 'src';
const finalDir = 'assets';

const onError = err => console.log(err);

async function clean() {
  return await del.sync([finalDir, `${pjson.name}.zip`]);
}

// Main style
function styleCSS() {
  const autoprefixer = require('autoprefixer');
  return src(`${sourceDir}/sass/style.scss`)
    .pipe($.plumber({ errorHandler: onError }))
    .pipe($.sourcemaps.init())
    .pipe($.sass({ includePaths: ['node_modules'] }))
    .pipe($.postcss([autoprefixer()]))
    .pipe($.sourcemaps.write('.'))
    .pipe(dest('.'))
    .pipe(server.stream());
}

function js() {
  return src(`${sourceDir}/js/index.js`)
    .pipe($.plumber({ errorHandler: onError }))
    .pipe(webpackStream(require('./webpack.config.js', webpack)))
    .pipe(dest(`${finalDir}/js/`))
    .pipe(server.stream());
}

function images() {
  return src(`${sourceDir}/images/**/*`)
    .pipe(
      $.cache(
        $.imagemin({
          interlaced: true,
          progressive: true,
          optimizationLevel: 4,
          svgoPlugins: [{ removeViewBox: true }, { cleanupIDs: false }]
        })
      )
    )
    .pipe(dest(`${finalDir}/images`));
}

function watchTask() {
  server({
    notify: false,
    port: 5000,
    proxy: pjson.proxy,
    injectChanges: true,
    files: [
      '**/*.php',
      `${sourceDir}/images/**/*`,
    ]
  });

  watch(`${sourceDir}/sass/**/*.scss`, styleCSS);
  watch(`${sourceDir}/js/**/*.js`, js);
}

async function build() {
  const cssnano = require('cssnano');

  await del.sync(['**/*.map']);

  return src(`${finalDir}/**/*`)
    .pipe($.if('*.js', $.uglify()))
    .pipe(
      $.if(
        '*.css',
        $.postcss([
          cssnano({
            discardComments: {
              removeAll: false
            },
            discardDuplicates: true,
            discardEmpty: true,
            minifyFontValues: true,
            minifySelectors: true
          })
        ])
      )
    )
    .pipe(dest(`${finalDir}/`));
}

function zip() {
  return src([
    '**/*',

    // include specific files and folders
    'screenshot.png',

    // exclude files and folders
    '!src',
    '!src/**/*',
    '!node_modules',
    '!node_modules/**/*',
    '!inc/custom_types/acf_imports',
    '!inc/custom_types/acf_imports/**/*',
    '!cache',
    '!cache/**/*',
    '!**/*.map',
    '!**/*.md',
    '!**/*.txt',
    '!gulpfile.js',
    '!webpack.config.js',
    '!package.json',
    '!yarn.lock',
    '!composer.json',
    '!composer.lock'
  ])
    .pipe($.zip(`${pjson.name}.zip`))
    .pipe(dest('.'));
}

exports.watch = series(clean, parallel(styleCSS, js, images), watchTask);
exports.build = series(
  clean,
  parallel(styleCSS, js, images),
  build
);
exports.zip = series(
  clean,
  parallel(styleCSS, js, images),
  build,
  zip
);
