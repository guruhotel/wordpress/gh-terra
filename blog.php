<?php
/**
 * Template Name: Blog
 */
get_header();
while(have_posts()): the_post();
?>

    <div class="background-color__white padding__small-section">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-6">
                    <h2 class="font-size__mega text-color__titles" data-aos="zoom-in"><?php the_title(); ?></h2>
                </div>
            </div>
        </div>
    </div>

    <section class="blog-posts">

        <div class="container-fluid wrap">
            <div class="row">
                <?php
                    $post_number = 1;
                    $paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
                    $args = (array(
                        'post_type' => 'post',
                        'posts_per_page' => 16,
                        'paged' => $paged,
                        ) );
                    $query = new WP_Query($args);
                    if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
                ?>
                    <article title="<?php the_title(); ?>" class="article col-md-4 margin-bottom__big <?php if($post_number == 1 ) echo 'article--big col-md-12 col-xs-12'; ?>" data-aos="fade-up" data-aos-delay="<?php echo $post_number*50; ?>">
                        <?php echo get_template_part( 'post-card', null ); ?>
                    </article>
                <?php $post_number++; endwhile; endif; ?>

                <div class="pagination margin-bottom__mega--x">
                    <?php
                      $big = 999999999; // need an unlikely integer

                      echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?page=%#%',
                        'current' => max( 1, get_query_var('page') ),
                        'total' => $query->max_num_pages,
                        'prev_text' => '<',
                        'next_text' => '>'
                      ) );
                    ?>
                </div>
                <?php wp_reset_postdata(); ?>
            </div>

        </div>

    </section>

<?php endwhile; get_footer();
