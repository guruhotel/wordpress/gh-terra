module.exports = {
  mode: 'development',
  devtool: 'source-map',
  output: {
    filename: 'main.js'
  },
  externals: {
    //Because Wordpress is already loading jQuery
    "jquery": "jQuery"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }
}
