<?php
get_header();
while(have_posts()): the_post();
?>

    <div class="container-fluid wrap">
        <div class="row center-xs">
            <div class="col-xs-12 col-sm-10 col-md-10 margin-top__medium start-xs">

                <div class="margin-top__big margin-bottom__mega--x">
                    <a class="btn font-weight__normal text-color__text btn__size--normal border-radius__normal background-color__grey" href="<?php echo bloginfo('wpurl'); ?>"><i class="fas fa-long-arrow-alt-left"></i> Artículos</a>
                </div>

                <article class="article-single border-color__grey--regent card background-color__white border-radius__medium card__size--mega box-shadow__small">
                    <?php
                        $categories = wp_get_post_terms( $post->ID, 'category');
                        if ($categories[0]->slug != 'uncategorized' && !is_archive()):
                    ?>
                        <div class="category__badge" style="background: <?php the_field('category_badge_color', $categories[0]) ?>;"><i class="<?php the_field('category_badge_icon', $categories[0]); ?>"></i><a href="<?php echo get_term_link( $categories[0], 'category' ); ?>"><?php echo $categories[0]->name; ?></a></div>
                    <?php endif; ?>


                    <div class="article-metas center-xs">
                        <time class="meta">
                            <time datetime="1592420560881"><?php echo get_the_date('M d, Y'); ?></time>
                        </time>
                    </div>
                    <h1 class="article-title center-xs font-size__mega"><?php the_title(); ?></h1>

                    <div class="article-share margin-top__mega margin-bottom__medium center-xs">
                        <a href="https://www.facebook.com/sharer.php?u=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-right__normal border-radius__rounded text-color__white btn__size--medium background-color__facebook">
                            <i class="fab fa-facebook-f"></i>
                        </a>

                        <a href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-right__normal border-radius__rounded text-color__white btn__size--medium background-color__twitter">
                            <i class="fab fa-twitter"></i>
                        </a>

                        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-right__normal border-radius__rounded text-color__white btn__size--medium background-color__blue">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </div>

                    <div class="article-thumb margin-top__mega--x border-radius__normal margin-bottom__mega--x">
                        <?php the_post_thumbnail('large', array('class' => 'border-radius__normal')); ?>
                    </div>

                    <div class="article-content"><?php the_content(); ?></div>

                    <div class="article-share margin-top__mega center-xs">
                        <div class="margin-bottom__mega">
                            <h4 class="font-size__small--x">Compartir artículo:</h4>
                        </div>

                        <a href="https://www.facebook.com/sharer.php?u=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-right__normal border-radius__rounded text-color__white btn__size--medium background-color__facebook">
                            <i class="fab fa-facebook-f"></i>
                        </a>

                        <a href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-right__normal border-radius__rounded text-color__white btn__size--medium background-color__twitter">
                            <i class="fab fa-twitter"></i>
                        </a>

                        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-right__normal border-radius__rounded text-color__white btn__size--medium background-color__blue">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </div>

                </article>
            </div>

            <div class="blog-posts blog-posts--related">
                <div class="col-xs-12 margin-top__mega--x">
                    <h4 class="margin-bottom__big">Otros artículos</h4>
                    <div class="row">
                        <?php
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = (array(
                                'post_type' => 'post',
                                'posts_per_page' => 6,
                                'paged' => $paged,
                                ) );
                            $query = new WP_Query($args);
                            if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
                        ?>
                            <article title="<?php the_title(); ?>" class="article col-md-4 margin-bottom__big" data-aos="fade-up" >
                                <?php echo get_template_part( 'post-card', null ); ?>
                            </article>
                        <?php endwhile; endif; wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>


<?php endwhile; get_footer();
