//Imports
import slick from 'slick-carousel';
import AOS from 'aos';

//Site code
jQuery(document).ready(function($){

    //Animations
    AOS.init();

    $('#toggle-search').click(function(event){
        $('.search-form-wrapper').toggleClass('active');
        $('.search-form-wrapper input[type=text]').focus();
    });
});

