
    <section class="pre-footer margin-top__mega--x padding-top__mega--x padding-bottom__mega--x background-color__main text-color__white">
        <img src="<?php bloginfo('template_directory'); ?>/assets/images/footer-illustration.svg" class="pre-footer-img">
        <div class="container-fluid wrap">
            <div class="row middle-xs">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-offset-1">
                    <h2 class="font-size__mega text-color__white"><?php the_field('footer_cta_title', 'option'); ?></h2>
                    <p><?php the_field('footer_cta_text', 'option'); ?></p>
                    <a href="<?php the_field('footer_cta_url', 'option'); ?>" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__normal font-size__small--x">Comenzar</a>
                </div>
            </div>
        </div>
    </section>

    <footer id="footer" class="padding-top__big padding-bottom__big background-color__grey">
        <div class="footer-bottom container-fluid wrap">
            <div class="row center-xs">
                <p class="font-size__small--x">© 2020 GuruHotel - Cadena Hotelera Digital</p>
            </div>
        </div>
    </footer>


<?php wp_footer(); ?>
</body>
</html>
