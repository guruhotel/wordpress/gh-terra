<?php $categories = wp_get_post_terms( $post->ID, 'category'); ?>
<div class="card card__size--big background-color__white border-radius__medium margin-bottom__big">
    <?php if ($categories[0]->slug != 'uncategorized' && !is_archive()): ?>
        <div class="category__badge" style="background: <?php the_field('category_badge_color', $categories[0]) ?>;"><i class="<?php the_field('category_badge_icon', $categories[0]); ?>"></i><a href="<?php echo get_term_link( $categories[0], 'category' ); ?>"><?php echo $categories[0]->name; ?></a></div>
    <?php endif; ?>

    <a href="<?php the_permalink(); ?>">
        <div class="article-thumb-wrap border-radius__medium">
            <div class="article-thumb">
                <?php the_post_thumbnail( 'medium'); ?>
            </div>
        </div>
        <div class="article-caption padding__big--x">
            <div class="article-metas margin-bottom__normal">
                <time class="meta font-size__small--x font-weight__normal text-color__text"><i class="far fa-calendar text-color__main"></i>
                <time datetime="1591292775453"><?php echo get_the_date('M d, Y'); ?></time></time>
            </div>
            <h2 class="article-title font-size__medium"><?php the_title(); ?></h2>
            <span class="btn__read font-weight__normal font-size__small--x text-color__main"><?php _e('Leer artículo', 'terra'); ?><i class="fas fa-long-arrow-alt-right margin-left__normal"></i>
            </span>
        </div>
    </a>
</div>
